#!/usr/bin/env bash

# Retrieves the PM names for product groups
#
# USAGE:
# ❯ find-product-manager.sh ~/code/www-gitlab-com Geo
#
# OUTPUT:
# Sampath Ranasinghe

scripts=$(dirname -- "$( readlink -f -- "$0")")
$scripts/find-stage-data.sh pm $@
