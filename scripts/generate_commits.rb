#!/usr/bin/env ruby

# frozen_string_literal: true

require 'git'
require 'logger'
require 'optimist'

OPTS = Optimist.options do
  banner 'Generate and commit some random files (small or large; in Git or LFS)'
  banner "and push them to origin's default branch."
  banner ''
  banner 'USAGE: gls_generate_commits [OPTIONS].'
  banner ''
  banner 'EXAMPLE 1: Stressing Gitaly a bit'
  banner 'gls_generate_commits --number=10 --megabytes=100 --push=each --cleanup'
  banner ''
  banner 'EXAMPLE 2: Testing https://docs.gitlab.com/ee/administration/housekeeping.html#housekeeping-options'
  banner 'gls_generate_commits --number=10 --megabytes=100 --push=each --cleanup'
  banner ''
  banner 'OPTIONS'
  opt :cleanup, 'Whether or not to remove the generated files.',
    short: '-c', default: false, type: :boolean
  opt :lfs, 'Whether the files should be in LFS instead of Git.',
    short: '-l', default: false, type: :boolean
  opt :number, 'How many commits to generate.',
    short: '-n', default: 1, type: :integer
  opt :megabytes, 'Specify, how large each commit should be.',
    short: '-m', default: 0, type: :integer
  opt :push, 'Whether or how to push the generated commits: once (default) or each.',
    short: '-p', default: 'once', type: :string
end

# We get the _relative_ path for further use, right after the _absolute_ one gets created.
PREFIX = '_tmp_random_'
DIR = Dir.mktmpdir(PREFIX, Dir.pwd)
         .sub(Dir.pwd, '').sub('/', '')

MEGABYTES = OPTS[:megabytes]
NUMBER = OPTS[:number]
PUSH = OPTS[:push]
LFS = OPTS[:lfs]

def commit_file!(git, path)
  git.add(path)
  git.commit("Add #{path}")
end

def write_file!(dir)
  random = Random.new.rand.to_s[2..]
  random += '.lfs' if LFS

  file = File.new("#{dir}/#{random}", 'w')
  file.write("#{random}\n")

  file.write("\n#{Random.new.bytes(MEGABYTES * (1024**2))}}\n") if MEGABYTES.positive?

  file.close
  file.path
end

def delete_files!(git)
  all_tmp = "#{PREFIX}*"
  git.remove(all_tmp)
  git.commit("Delete #{all_tmp}")
end

### Main

git = Git.open(Dir.pwd, log: Logger.new($stdout))

if LFS && !File.exist?('.gitattributes')
  system('git lfs install && git lfs track "*.lfs"')
  git.add('.gitattributes')
  git.commit('Track .lfs files')
end

NUMBER.times do |i|
  puts "\n\n\n### Commit #{i + 1} of #{NUMBER}\n\n"

  path = write_file!(DIR)
  commit_file!(git, path)
  git.push(git.remotes.first, git.current_branch) if PUSH == 'each'
end

if OPTS[:cleanup]
  puts "\n\n\n### Cleanup\n\n"
  delete_files!(git)
  git.push(git.remotes.first, git.current_branch) if PUSH == 'each'
end

git.push(git.remotes.first, git.current_branch) if PUSH == 'once'
