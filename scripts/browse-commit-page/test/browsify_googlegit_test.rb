# frozen_string_literal: true

require 'minitest/autorun'
require_relative './browsifier_test'

class BrowsifyGoogleGitTest < BrowsifierTest
  # Not to be confused with https://opensource.google/
  # their marketing/evangelism landing page.

  # We mix known subdomains into the other protocol test case
  # just to confirm that Browsifier generally ignores subdomains.

  def test_google_git_http
    remote = 'https://git.googlesource.com/n-s/sg/prj.git'
    assert_equal "https://git.googlesource.com/n-s/sg/prj/+/#{@sha}",
      Browsifier.for(remote).commit_page(@sha)
  end

  def test_google_git_ssh
    remote = 'git@code.googlesource.com:n-s/sg/prj.git'
    assert_equal "https://code.googlesource.com/n-s/sg/prj/+/#{@sha}",
      Browsifier.for(remote).commit_page(@sha)
  end

  def test_google_git
    remote = 'git://kernel.googlesource.com/n-s/sg/prj.git'
    assert_equal "https://kernel.googlesource.com/n-s/sg/prj/+/#{@sha}",
      Browsifier.for(remote).commit_page(@sha)
  end
end
