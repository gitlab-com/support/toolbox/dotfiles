# frozen_string_literal: true

require 'minitest/autorun'
require_relative './browsifier_test'

class BrowsifyDefaultsTest < BrowsifierTest
  def test_directory_remote_raises_error
    remote = 'guser@git.example.com:/srv/git/my_project.git'
    assert_raises { Browsifier.for(remote).httpsify }
  end

  # Default tests include the "\n" that Open3.capture3(REMOTE_CMD) keeps appended
  def test_default_http
    remote = "https://github.com/ns/prj.git\n"
    assert_equal "https://github.com/ns/prj/commit/#{@sha}",
      Browsifier.for(remote).commit_page(@sha)
  end

  def test_default_ssh
    remote = "git@codeberg.org:ns/prj.git\n"
    assert_equal "https://codeberg.org/ns/prj/commit/#{@sha}",
      Browsifier.for(remote).commit_page(@sha)
  end
end
