# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../lib/browsifier'

class BrowsifierTest < Minitest::Test
  def setup
    @sha = '09af'
    # Just a short placeholder for thr character range.
    # Is inherited by other test/*_test.rb files.
  end

  # We don't need HTTP _and_ SSH tests for each platform

  def test_gitlab_ssh
    remote = 'git@gitlab.com:parent/sub-group/project.git'
    assert_equal "https://gitlab.com/parent/sub-group/project/-/commit/#{@sha}",
      Browsifier.for(remote).commit_page(@sha)
  end

  def test_bitbucket
    remote = 'https://bitbucket.org/namespace/project.git'
    assert_equal "https://bitbucket.org/namespace/project/commits/#{@sha}",
      Browsifier.for(remote).commit_page(@sha)
  end

  def test_gitweb_git
    remote = 'git://git.kernel.org/ns/prj.git'
    assert_equal "https://git.kernel.org/ns/prj.git/commit/?id=#{@sha}",
      Browsifier.for(remote).commit_page(@sha)
  end
end
