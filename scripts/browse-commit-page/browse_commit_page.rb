#!/usr/bin/env ruby
# frozen_string_literal: true

require 'open3'
require_relative './lib/browsifier'

# Fetches Remote URL &
module BrowseCommitPage
  REMOTE_NAME = ENV['REMOTE_NAME'] || 'origin'
  REMOTE_CMD  = "git config --get remote.#{REMOTE_NAME}.url"

  def self.construct(sha)
    Browsifier.for(remote).commit_page(sha)
  end

  def self.remote
    output = Open3.capture3(REMOTE_CMD)
    raise(output[1]) unless output[1].empty?
    raise(ArgumentError, 'No Git remote configured!') if output[0].empty?

    output[0].strip
  end
end

commit_id = ARGV.pop
Kernel.system "open #{BrowseCommitPage.construct(commit_id)}"
