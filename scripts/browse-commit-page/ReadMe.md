# URLify Commit IDs

Ruby implementation of a cool tip shared by @manuelgrabowski
in his [personal blog](https://log.manuelgrabowski.de/post/open-commit-hashes-from-iterm/).
Extensible to support multiple code hosting platforms

## Installation / Configuration

1. See [top-level ReadMe](../../README.md#installation)
2. In iTerm2's [Smart Selection](https://iterm2.com/documentation-smart-selection.html) 
   menu, add one such rule with the reg-ex `[0-9a-f]{4,40}` (or similar).
3. Select `Run Command`/`Coprocess` and add:

   ```shell
   cd "\d" && path/to/browse_commit_page.rb "\0"
   # or path/to/bin/gls_browse_commit_page
   ```

## Usage

Point, click & browse :-)

If your remote isn't called `origin`, supply its name with the environment variable `REMOTE_NAME`
(in exactly the same capitalization as `git remote -v` shows).

### Caveats

- See [`./test` folder](./test/) for the list of supported platforms.
- `Run Command in window` also works, but only if you've set your iTerm2 profile
   to `Reuse previous session's directory`.

## Contributing

MRs are welcome! Esp. more `Browsify`… code and tests for more Git hosters.  
For major changes, please first open an issue to discuss your suggestion.

## License: [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

Copyright (c) 2021 Manuel Grabowski & Katrin Leinweber (GitLab Inc.)
