# Turn all kinds of remote URLs into browsable URLs
# Defaults patterns match GitHub & Codeberg.
class Browsifier
  attr_reader :remote

  def initialize(remote)
    @remote = remote
  end

  def self.for(remote)
    registry
    .find { |subclass| subclass.handles?(remote) }
    .new(remote)
  end

  def commit_page(sha)
    "#{remote_base}/commit/#{sha}"
  end

  private

  def self.registry
    @registry ||= [Browsifier]
  end

  def self.inherited(subclass)
    register(subclass)
  end

  def self.register(subclass)
    registry.prepend(subclass)
  end

  def self.handles?(remote)
    remote.match? /(codeberg.org)|(github.com)/ || true
  end

  # TODO: Detect type of self-hosted platforms (behind custom domains)

  def remote_base
    httpsify.strip.chomp(".git")
  end

  # Converts all remote URL types to https://
  def httpsify
    raise "Non-platform, directory-based URL: #{remote}. Can't browse!" if remote.match? DIR_PATTERN
      
     remote.match? SSH_PATTERN
      remote.sub(/\Agit/, 'https').sub(/:/, '/').sub(/@/, '://')
      # TODO: other usernames?
    elsif remote.match? GIT_PATTERN
      remote.sub(/\Agit:/, 'https:')
    elsif remote.match? HTT_PATTERN
      # There's probably a reason for still using http://,
      # which also makes a redirect back from https:// unlikely.
      # Better to pass through both protocols unmodified.
      remote
    else
      raise "Unknwon URL pattern: #{remote}. Is it supported? See #{SUPPORTED}!"
    
  end

  SUPPORTED = 'https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes#_showing_your_remotes'
  DIR_PATTERN = /\w+@.*:\W/
  GIT_PATTERN = /\Agit:/
  SSH_PATTERN = /\Agit@.*:/
  HTT_PATTERN = /https?:/
end

# Special case due to https://gitlab.com/gitlab-org/gitlab/-/issues/214217
class BrowsifyGitLab < Browsifier
  def commit_page(sha)
    remote_base + '/-/commit/' + sha
  end

  def self.handles?(remote)
    remote.match?('gitlab.com')
  end
end

class BrowsifyBitbucket < Browsifier
  def commit_page(sha)
    remote_base + '/commits/' + sha
  end

  def self.handles?(remote)
    remote.match?('bitbucket.org')
  end
end

class BrowsifyGoogleGit < Browsifier
  def commit_page(sha)
    remote_base + '/+/' + sha
  end

  def self.handles?(remote)
    remote.match?('googlesource.com')
  end
end

class BrowsifyKernelOrg < Browsifier
  def commit_page(sha)
    httpsify + '/commit/?id=' + sha
  end

  def self.handles?(remote)
    remote.match?('git.kernel.org')
  end
end
