#!/usr/bin/env ruby

# frozen_string_literal: true

require 'gitlab'
require 'optimist'

opts = Optimist.options do
  banner "Usage: ruby request-for-help-issue.rb [options]"
  opt :search, "Partial and case insensitive group name search term", short: '-s', required: true, type: String
end

# Get the access token from environment variables
access_token = ENV['GITLAB_API_PRIVATE_TOKEN'] || ENV['LAB_CORE_TOKEN']
search_param = opts[:search]

# Ensure access token is provided
if access_token.nil?
  puts "Please set the access token in the GITLAB_API_PRIVATE_TOKEN or LAB_CORE_TOKEN environment variable."
  exit
end

Gitlab.configure do |config|
  config.endpoint = 'https://gitlab.com/api/v4'
  config.private_token = access_token
end

# Get all RFH issue template URLs
template_urls = []
project = Gitlab.project('gitlab-com/request-for-help')
files = Gitlab.tree(project.id, path: '.gitlab/issue_templates', per_page: 100)

# Store the URL for each file in the array
files.each do |file|
  template_url = "https://gitlab.com/gitlab-com/request-for-help/-/issues/new?issuable_template=#{file['name'].gsub(
    '.md', '')}"
  template_urls << template_url
end

# Filter and output the array elements based on the fuzzy search parameter
matched_urls = template_urls.select { |url| url.downcase.include?(search_param.downcase) }

# Output the matched URLs
matched_urls.each do |url|
  puts url
end
