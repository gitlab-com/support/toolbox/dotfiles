Please see either the [top-level README.md](../README.md),
or each sub-folder's dedicated one,
or the documentation comments in each script file.
