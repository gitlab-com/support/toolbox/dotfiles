#!/usr/bin/env bash

# USAGE EXAMPLES:
#
# $ gls_search_default_gitlab_rb_version \
#     ~/GitLab.com/omnibus-gitlab \
#     13.12.10 \
#     puma.+worker
#
# $ ./search_default_gitlab_rb_version.sh \
#     ~/GitLab.com/omnibus-gitlab \
#     17.2.1 \
#     'aws_(access|secret)' \
#      3
#
# Or via a local alias (see https://gitlab.com/katrinleinweber/dotfiles/-/commit/5df0a083):
#
# $ glgrb 17-2 puma.+worker

omnibus_folder="$1" # e.g. ~/GitLab.com/GDK/omnibus-gitlab
file=files/gitlab-config-template/gitlab.rb.template

# Determine the Git reference type: https://superuser.com/a/795465
if [[ $(grep -cE '^\d+\.\d+.\d+$' <<<"$2") -eq '1' ]]; then
  version="$2"+ee.0
  # semantic version tag
elif [[ $(grep -cE '^\d+-\d+' <<<"$2") -eq '1' ]]; then
  version="origin/$2"-stable
  # minor release branch
else
  version="$2"
  # branch name verbatim
fi

pattern="$3"
context="${4:-0}"
# This order IMHO helps when reusing commands from shell history.
# Likely, one searches multiple items of the same Omnibus version.

git -C "$omnibus_folder" grep -h \
  -C "$context" \
  --ignore-case \
  --extended-regexp "$pattern" \
  "$version" \
  -- \
  "$file"
