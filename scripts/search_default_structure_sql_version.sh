#!/usr/bin/env bash

# USAGE EXAMPLES:
#
# $ gls_search_default_structure_sql_version \
#     ~/GitLab.com/GDK/gitlab \
#     13.12.10 \
#     label_.+
#
# Or via a local alias (see https://gitlab.com/katrinleinweber/dotfiles/-/commit/5df0a083):
#
# $ glsql 17-2 label_.+

gitlab_folder="$1" # e.g. ~/GitLab.com/GDK/gitlab
file=db/structure.sql

# Determine the Git reference type: https://superuser.com/a/795465
if [[ $(grep -cE '^\d+\.\d+.\d+$' <<<"$2") -eq '1' ]]; then
  version=v"$2"-ee
  # semantic version tag
elif [[ $(grep -cE '^\d+-\d+' <<<"$2") -eq '1' ]]; then
  version="$2"-stable-ee
  # minor release branch
else
  version="$2"
  # branch name verbatim
fi

pattern="$3"
# This order IMHO helps when reusing commands from shell history.
# Likely, one searches multiple items of the same GitLab version.

git -C "$gitlab_folder" grep -h \
  --ignore-case \
  --function-context \
  --extended-regexp "$pattern" \
  "$version" \
  -- \
  "$file"
