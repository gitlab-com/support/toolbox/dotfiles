#!/usr/bin/env ruby
# frozen_string_literal: true

# Simple script by Kamil Trzciński to parse over Gitaly logs and generate statistics.
# Similar to https://gitlab.com/gitlab-com/support/toolbox/fast-stats/

require 'json'

KEYS = ['grpc.request.glProjectPath', 'grpc.request.repoStorage', 'grpc.method'].freeze # string keys
VALUES = ['command.real_time_ms'].freeze # numeric keys

sum = Hash.new { [] }

# Read all log files passed
ARGV.each do |file|
  File.readlines(file).each do |line|
    json = JSON.parse(line)
    keys = json.slice(*KEYS)
    values = json.slice(*VALUES)
    sum[keys] <<= values
  end
end

# Add all VALUES
sum.transform_values! do |all_values|
  result = all_values.each_with_object(Hash.new { [] }) do |values, sum|
    values.each { |key, value| sum[key] <<= value }
  end

  result.transform_values do |values|
    values.sort!

    {
      sum: values.sum,
      count: values.count,
      avg: values.sum.to_f / values.count,
      p50: values[values.count * 50 / 100],
      p95: values[values.count * 95 / 100],
      p99: values[values.count * 99 / 100]
    }
  end
end

# Print output
sum.each do |keys, values|
  line = keys.values_at(*KEYS).map { |key| key || '?' }
  line += values.values_at(*VALUES).map do |value|
    (value || {}).values_at(:sum, :count, :avg, :p50, :p95, :p99).map(&:to_i)
  end
  puts(line.join("\t"))
end
