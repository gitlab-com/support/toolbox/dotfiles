#!/usr/bin/env bash

### Read release posts for a given Major.ninor version
#
### PREREQUISITES
#
#   1. Clone https://gitlab.com/gitlab-com/www-gitlab-com/
#   2. Install yq, see https://github.com/mikefarah/yq/#install
#
### USAGE EXAMPLES
#
#   gls_read_release_posts ~/GitLab.com/www 17.5
#
### via local alias of ⬆️
#
#   rrp 17.5

path_to_www_clone="$1"
release_version="$2"

base_dir_post='sites/uncategorized/source/releases/posts'
base_dir_data='data/release_posts'

function patch_version_input() {
  [[ $(echo "$1" | tr '.' '\n' | wc -l | rev | cut -c1) -ne 2 ]]
}

if patch_version_input "$release_version"; then
  echo "Only  Major.minor  version input is supported. Remove .patch and try again!"
  exit 1
fi

# Print warning in case of >1 day old local data
mod=$(date -r "$path_to_www_clone"/"$base_dir_post" +"%s")
now=$(date +"%s")
dif=$((now - mod))
if [ "$dif" -ge 86400 ]; then
  echo "WARNING: Local file <$path_to_www_clone/$base_dir_post> not changed since yesterday"
  echo "  Trying to checkout latest version..."
fi

if ! git -C "$path_to_www_clone" fetch --all &>/dev/null; then
  echo "Can't fetch latest repo content for  $path_to_www_clone  :-("
  exit 1
else
  git -C "$path_to_www_clone" checkout origin/HEAD &>/dev/null
fi

dir_posts="$path_to_www_clone/$base_dir_post"
dir_yamls="$path_to_www_clone/$base_dir_data"
glob_post="$dir_posts/*gitlab-${release_version/./-}*released.html.md"
glob_yaml="$dir_yamls/${release_version/./_}/*"
yaml_path='.*.[].[].description'

eval "cat $glob_post"
eval "yq  $yaml_path $glob_yaml"
