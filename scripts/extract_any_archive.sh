#!/usr/bin/env bash

set -u
set -e
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
set -o pipefail
IFS=$'\n\t'

extract_any_archive()  {
  for archive in "$@"; do
    if [ -f "$archive" ]; then
      case "$archive" in
        *.tar.bz2)   tar xvjf "$archive"     ;;
        *.tar.gz)    tar xvzf "$archive"     ;;
        *.bz2)       bunzip2 "$archive"      ;;
        *.rar)       rar x "$archive"        ;;
        *.gz)        gunzip "$archive"       ;;
        *.tar)       tar xvf "$archive"      ;;
        *.tbz2)      tar xvjf "$archive"     ;;
        *.tgz)       tar xvzf "$archive"     ;;
        *.zip)       unzip "$archive"        ;;
        *.Z)         uncompress "$archive"   ;;
        *.7z)        7z x "$archive"         ;;
        *.s)         gunzip -S .s "$archive" ;;
        *)           echo "don't know how to extract '$archive'..." ;;
      esac
    else
      echo "'$archive' is not a known archive type"
    fi
  done
}

extract_any_archive "$@" 2 &>/dev/null
