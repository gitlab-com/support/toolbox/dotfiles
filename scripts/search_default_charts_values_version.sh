#!/usr/bin/env bash

# PREREQUISITE: Install yq, see https://github.com/mikefarah/yq/#install
#
# USAGE EXAMPLES:
#
# $ gls_search_default_charts_values_version \
#     ~/GitLab.com/charts-gitlab \
#     4.12.10 \
#     logPath
#
# Or via a local alias (see https://gitlab.com/katrinleinweber/dotfiles/-/commit/161adae0):
#
# $ glchv 4.12.10 logPath
#
# Note that the charts version tag = GitLab Version - 9,
# see https://docs.gitlab.com/charts/installation/version_mappings.html

charts_folder="$1"
file=values.yaml

version=v"$2"
pattern="$3"
# This order IMHO helps when reusing commands from shell history.
# Likely, one searches multiple items of the same Charts version.

git -C "$charts_folder" \
  cat-file --textconv "$version":"$file" |
  yq eval --output-format=props |
  grep \
    --ignore-case \
    --extended-regexp "$pattern"
