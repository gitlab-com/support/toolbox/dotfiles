#!/usr/bin/env bash

# USAGE EXAMPLE:
#
# $ gls_search_sidekiq_worker_data \
#     ~/GitLab.com/GDK/gitlab \
#     16.11.1 \
#     search_.+

gitlab_folder="$1" # e.g. ~/GitLab.com/GDK/gitlab
yml1=app/workers/all_queues.yml
yml2=ee/app/workers/all_queues.yml

version=v"$2"-ee
pattern="$3"
# This order IMHO helps when reusing commands from shell history.
# Likely, one searches multiple items of the same GitLab version.

for file in "$yml1" "$yml2"; do
  echo -e "\n# $file"
  git -C "$gitlab_folder" \
    cat-file  --textconv "$version":"$file" |
    grep --before-context=2 --after-context=5 --ignore-case --extended-regexp "$pattern" |
    grep --invert-match 'tags: \[\]'
done
