#!/usr/bin/env ruby

# frozen_string_literal: true

require 'gitlab'
require 'optimist'

EXTRACTION_REGEX = %r{https://gitlab\.com/(.*)/-/(\w*)/(\d*)(#note_(\d*))?}.freeze

def award_emoji(emoji_shortcode, url)
  match = EXTRACTION_REGEX.match(url)

  if match
    project_name = match[1]
    awardable_type = match[2][0...-1] # stripping the plural 's' at the end
    awardable_id = match[3]
    comment_id = match[5]

    add_award_emoji(project_name, awardable_type, awardable_id, comment_id, emoji_shortcode)
  else
    puts 'Invalid URL.'
  end
end

def add_award_emoji(project_name, awardable_type, awardable_id, comment_id, emoji_shortcode)
  if comment_id
    Gitlab.create_note_award_emoji(project_name, awardable_id, awardable_type, comment_id, emoji_shortcode)
  else
    Gitlab.create_award_emoji(project_name, awardable_id, awardable_type, emoji_shortcode)
  end

  puts "Emoji '#{emoji_shortcode}' awarded successfully!"
rescue Gitlab::Error::NotFound
  puts "Emoji '#{emoji_shortcode}' already awarded."
rescue StandardError => e
  puts "Emoji '#{emoji_shortcode}' could not be awarded: " + e.message
end

# Set up commandline options
opts = Optimist.options do
  banner 'Usage: ruby emoji.rb [options]'
  opt :"emoji-shortcode", 'Award emoji shortcode', type: String, required: true, short: 'e'
  opt :url, 'GitLab issue or merge request URL', type: String, required: true, short: 'u'
end

# Get access token from environment variables
access_token = ENV['GITLAB_API_PRIVATE_TOKEN'] || ENV['LAB_CORE_TOKEN']

# Ensure access token is provided
if access_token.nil?
  puts 'Please set the access token in the GITLAB_API_PRIVATE_TOKEN or LAB_CORE_TOKEN environment variable.'
  exit
end

# Set up GitLab API connection
Gitlab.configure do |config|
  config.endpoint = 'https://gitlab.com/api/v4'
  config.private_token = access_token
end

# Execute script
award_emoji(opts[:"emoji-shortcode"], opts[:url])
