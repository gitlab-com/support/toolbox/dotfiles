#!/usr/bin/env ruby

# frozen_string_literal: true

# Instantiate a docker-compose.yml for a GitLab test instance, as per
# https://docs.gitlab.com/ee/install/docker.html#install-gitlab-using-docker-compose
#
# ## USAGE: Read the --help flag output for details.
#
# ## EXAMPLE
#   gls_compose_docker_instance \
#     --name kl-654321-test \
#     --version-tag 14.7.0 \
#     --license ~/path/to/Ultimate.gitlab-license \
#     --up
#
# Migrated from an earlier Bash version
# https://gitlab.com/katrinleinweber/dotfiles/-/blame/42aa249b/dot_config/functions.tmpl#L167-192

require 'fileutils'
require 'optimist'
require 'securerandom'

opts = Optimist.options do
  banner 'USAGE: compose_docker_instance.rb [OPTIONS].'
  opt :name, 'Instance name (if you include a ticket ID, its last 2 digits will be used to generate pseudorandom ports)',
    short: '-n', type: :string
  opt :license, 'Path to a .gitlab-license file. It will be copied to the /etc/config volume, see https://docs.gitlab.com/ee/user/admin_area/license.html#add-your-license-during-installation',
    short: '-l', type: :string
  opt :version_tag, 'Numeric GitLab version. Will be normalised to the required tag syntax: https://hub.docker.com/r/gitlab/gitlab-ee/tags',
    short: '-v', default: 'latest', type: :string
  opt :up, 'Spin up the composed instance, instead of editing the docker-compose.yml first.',
    short: '-u', default: false
end

FILENAME = 'docker-compose.yml'
TICKET_ID = /\d{6}/.freeze

SUFFIX =
  case opts[:name]
  when TICKET_ID
    opts[:name].scan(TICKET_ID).first.split('')[4..5].join
  else
    Random.random_number(10..99)
  end

BASE_DIR =
  case RbConfig::CONFIG['host_os']
  when /darwin|mac os/
    "#{ENV['HOME']}/gitlab/#{opts[:name]}"
  when /linux/
    "/srv/gitlab/#{opts[:name]}"
  else
    raise "Unsupported operating system: #{RbConfig::CONFIG['host_os']}"
  end

FULL_TAG = /\A\d+(\.\d+){2}-ee.0\z/.freeze
NUM_ONLY = /\A\d+(\.\d+){2}\z/.freeze

VERSION =
  case opts[:version_tag]
  when 'latest'
    opts[:version_tag]
  when FULL_TAG
    opts[:version_tag]
  when NUM_ONLY
    "#{opts[:version_tag]}-ee.0"
  else
    raise "Invalid version tag: #{opts[:version_tag]}. See --help for instructions."
  end

HTTP_PORT = "80#{SUFFIX}"
SSH_PORT  = "22#{SUFFIX}"

TEMPLATE = <<~HEREDOC
  version: '3.6'
  services:
    web:
      image: gitlab/gitlab-ee:#{VERSION} # Not yet supported on M1, see https://gitlab.com/groups/gitlab-org/-/epics/2370
      restart: on-failure
      hostname: 'localhost'
      environment:
        GITLAB_ROOT_PASSWORD: '#{SecureRandom.alphanumeric(16)}'
        GITLAB_OMNIBUS_CONFIG: |
          external_url 'http://localhost:#{HTTP_PORT}'
          gitlab_rails['gitlab_shell_ssh_port'] = #{SSH_PORT};
          gitlab_rails['usage_ping_enabled'] = false
      ports:
        - '#{HTTP_PORT}:#{HTTP_PORT}'
        - '#{SSH_PORT}:22'
      volumes:
        - '#{BASE_DIR}/config:/etc/gitlab'
        - '#{BASE_DIR}/logs:/var/log/gitlab'
        - '#{BASE_DIR}/data:/var/opt/gitlab'
      shm_size: '256m'

  #{'# To launch this instance, run:' unless opts[:up]}
  #{'# docker-compose up --detach'    unless opts[:up]}
  #{'# docker-compose logs --follow'  unless opts[:up]}

HEREDOC

File.write(FILENAME, TEMPLATE)

# Prepare folder structure & pre-load license file
if opts[:license]
  FileUtils.mkdir_p("#{BASE_DIR}/config/")
  FileUtils.cp(opts[:license], "#{BASE_DIR}/config/GitLab.gitlab-license")
end

if opts[:up]
  Kernel.system('docker-compose up -d')
else
  # Open the templated file for further modification
  Kernel.system("$EDITOR #{FILENAME}")

  # Pre-load the image
  Kernel.system("docker pull gitlab/gitlab-ee:#{VERSION}")
end
