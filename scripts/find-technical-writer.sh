#!/usr/bin/env bash

# Retrieves the TW names for product groups
#
# USAGE:
# ❯ find-technical-writer.sh ~/code/www-gitlab-com Pipeline Authoring
#
# OUTPUT:
# Marcel Amirault

scripts=$(dirname -- "$( readlink -f -- "$0")")
$scripts/find-stage-data.sh tech_writer $@
