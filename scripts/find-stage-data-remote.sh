#!/usr/bin/env bash

# Retrieves the names of certain roles for product groups
# Requires internet connection, use find-stage-data.sh
# to query a local stages.yml clone instead.
#
# USAGE:
# ❯ find-stage-data-remote.sh pm
#
# OUTPUT:
# Sampath Ranasinghe
# (the PM for the Geo group)

rolekey=$1 # e.g. pm, tech_writer, ux
shift

# using shift allows us to use "$*" (all parameters as one string) below,
# so you can do "tw pipeline authoring" instead of "tw 'pipeline authoring'"

curl -s 'https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/stages.yml' | yq '.stages.[].groups.[] | select(.name | test("(?i)'"$*"'")) | {.name: [] + .'"$rolekey"' | join(", ")}'
