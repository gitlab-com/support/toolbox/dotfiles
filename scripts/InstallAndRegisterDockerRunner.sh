#!/bin/bash

# `Required` options:
# -g : GitLab URL.
# -t : Runner registration token.
# -v : Tag name of the Runner image to be installed. Refer: https://hub.docker.com/r/gitlab/gitlab-runner/tags for tags.

# Example usage:
# ./InstallAndRegisterDockerRunner.sh -g https://gitlab.example.com -t a1b2c3d4e5f6g7 -v v14.10.1

# Exit on first error
set -eu

while getopts ":g:t:v:" opt; do
  case $opt in
    g) g="$OPTARG"
    ;;
    t) t="$OPTARG"
    ;;
    v) v="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    exit 1
    ;;
  esac
done

sudo apt-get update;
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install vim curl openssh-server ca-certificates apt-transport-https gnupg-agent software-properties-common;
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io

docker volume create gitlab-runner-config

if docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:$v
  then
    echo "Runner container has been started!"
  else
    echo "Failed to pull Runner image or start the container!"
    exit 1
fi

if docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:$v register --non-interactive --url "$g" --token "$t" --description 'Runner in Docker container' --executor 'docker' --docker-image 'docker:stable' --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" &> /dev/null
 then
   echo "Runner has been registered succesfully!"
 else
   echo "Runner registration failed! Check container logs!"
   exit 1
fi
