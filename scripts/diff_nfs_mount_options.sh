#!/usr/bin/env bash

# PREREQUISITE: Cloned gitlab-org/gitlab repo
#
# USAGE EXAMPLE
#
# $ gls_diff_nfs_mount_options \
#     ~/GDK/gitlab             \
#     15.11.0                  \
#     '/custom/mount/path'     \
#   | grep --extended-regexp '[<|>]' # to only show differences
#
# NOTE: NFS cannot be used for repository storage.
#       https://docs.gitlab.com/ee/administration/nfs.html
#
#       Updating mount options can help mitigate Git(aly) problems,
#       but the recommended solution is meeting these requirements:
#       https://docs.gitlab.com/ee/administration/gitaly/#disk-requirements

gitlab_folder="$1"
file='doc/administration/nfs.md'
term='/git-data nfs'

version=v"$2"-ee
mnt_name="$3"

function columnize_options {
  sed 's;[:, ];\n;g' |
    grep --invert-match "$file" |
    sed \
      -e 's;nfsvers;vers;g' \
      -e 's;lookupcache=pos\b;lookupcache=positive;g' |
    sort
}

for fstb in */etc/fstab; do
  if ! grep "$mnt_name" "$fstb" >/dev/null; then
    continue
  fi

  name=$(echo "$fstb" | sed -E 's;_20[0-9]+/;*/;')
  printf "\n\n### Diffing ↙️ our mount options doc against the customer's %s ↘️\n" "$name"

  diff --side-by-side \
    <(
      git -C "$gitlab_folder" \
        grep --fixed-strings "$term" "$version":"$file" |
        cut -d':' -f3- |
        columnize_options
    ) \
    <(
      grep "$mnt_name" "$fstb" |
        grep --invert-match '^\s*#' |
        columnize_options
    )
done
