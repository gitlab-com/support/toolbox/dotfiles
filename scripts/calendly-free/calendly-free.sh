#!/bin/bash
# calendly-free.sh
# Simple bash to fetch and print your calendly availability. TM 10/01/25
# Some elements based on the work in calendly-link in this project.

set -e
# We should fail if vars aren't set, so let's quietly check we have the op binary
op > /dev/null 2>&1  && OP_VERSION=$(op --version 2>/dev/null)
# Sometimes we have GNU date on MacOS, so let's try and check this
date -u -v+1d > /dev/null 2>&1 && date_type=bsd || date_type=gnu

if [[ "$CALENDLY_TOKEN" == "" ]] && [[ "$OP_VERSION" =~ ^2\. ]] && [[ "$DOTFILES_USE_1PASSWORD" == "true" ]]; then
    # Use 1Password CLI only if $1 and $CALENDLY_TOKEN are not set
    op account get >/dev/null 2>&1 || eval "$(op signin)"
    CALENDLY_TOKEN=$(op item get CALENDLY_TOKEN --vault Private --fields=password --reveal)
elif [[ -z "$CALENDLY_TOKEN" ]]; then
    echo "Error: CALENDLY_TOKEN environment variable is not set."
    exit 1
fi

pat=${1:-"$CALENDLY_TOKEN"}


get_available_slots() {

    # Grab our user_uri
    user_uri=$(curl -s \
    -H "Authorization: Bearer $pat" \
    -H 'Content-Type: application/json' \
    'https://api.calendly.com/users/me' \
    | jq -r '.resource.uri')

    # Calculate start time (tomorrow at 9:00 AM UTC) - the calendly api can only fetch 7 days at a time.
    if [[ "$date_type" == "gnu" ]]; then
        start_date=$(date -u -d tomorrow +"%Y-%m-%dT09:00:00.000000Z")
        end_date=$(date -u -d "+8 days" +"%Y-%m-%dT09:00:00.000000Z")
    elif [[ "$date_type" == "bsd" ]]; then
        start_date=$(date -u -v+1d -v9H +"%Y-%m-%dT%H:%M:%S.000000Z")
        end_date=$(date -u -v+8d -v9H +"%Y-%m-%dT%H:%M:%S.000000Z")
    fi


    # fetch the event types
    event_types=$(curl -s \
    -H "Authorization: Bearer $pat" \
    "https://api.calendly.com/event_types?user=$user_uri&active=true")

    # Prompt for the event to print
    echo "$event_types" | jq -r '.collection[] | [.name] | @tsv' | cat -n
    echo -e "\nEnter the number of the event you want to schedule:"
    read -r selection

    # And then fetch that availability
    index=$((selection - 1))
    event_type_uri=$(echo "$event_types" | jq -r ".collection | .[$index] | .uri")
    duration_text=$(echo "$event_types" | jq -r ".collection | .[$index] | .name")

    # Print out the block of availability
    response=$(curl -s -H "Authorization: Bearer $pat" \
        "https://api.calendly.com/event_type_available_times?user=$user_uri&event_type=$event_type_uri&start_time=$start_date&end_time=$end_date")

    echo -e "\nAvailable times for $duration_text"
    echo "------------------------"
    echo "$response" | jq -r '.collection[] | .start_time' | while read -r slot; do
        # Remove the 'Z' from the end of the date string
        slot_without_z="${slot%Z}"
        if [[ "$date_type" == "gnu" ]]; then
            slot_date=$(date -d "${slot_without_z}" "+%Y-%m-%d")
            slot_time=$(date -d "${slot_without_z}" "+%H:%M")
        elif [[ "$date_type" == "bsd" ]]; then
            slot_date=$(date -jf "%Y-%m-%dT%H:%M:%S" "${slot_without_z}" "+%Y-%m-%d")
            slot_time=$(date -jf "%Y-%m-%dT%H:%M:%S" "${slot_without_z}" "+%H:%M")
        fi

    echo "$slot_date $slot_time"
    done | sort | awk '{
        if (date != $1) {
            if (NR > 1) print "";
            date = $1;
            printf "%s: %s", date, $2;
        } else {
            printf ", %s", $2;
        }
    }
    END {
        print "";
    }'
    echo "------------------------"
}

# In case we want to fetch more than one duration?
fetch_another_meeting_type() {
    while true; do
        read -r -p "Fetch another meeting type, or quit? (y/q) " choice
        case "$choice" in
            y|Y ) get_available_slots ;;
            q|Q|n|N ) echo "Exiting.."; exit 0 ;;
            * ) echo "Invalid input. Please enter y or q." ;;
        esac
    done
}

get_available_slots
fetch_another_meeting_type
