# List your free Calendly meeting times

## Installation

0. Follow the Calendly account setup as described on
   [our handbook's Calendly page](https://about.gitlab.com/handbook/support/workflows/calendly.html).
1. Create a [token for their API](https://calendly.com/integrations/api_webhooks).
2. (Optional) To use with 1password-cli, install
   [1Password CLI package](https://app-updates.agilebits.com/product_history/CLI2). Version 2 is a requirement.

## Usage

### To provide Calendly API token **directly** export the environment variable `CALENDLY_TOKEN`,

```shell
$ export CALENDLY_TOKEN=yourlongapitokengoeshere
$ ./calendly-free.sh

```

### To use **1password-cli** to get the token instead:

1. Create a new login item named `CALENDLY_TOKEN` in your `Private` 1password vault, put the token into the `password` field
1. [Add your 1Password account to 1password cli](https://developer.1password.com/docs/cli/sign-in-manually)
1. Make sure to set `DOTFILES_USE_1PASSWORD=true` environment variable

```shell
$ export DOTFILES_USE_1PASSWORD=true
$ ./calendly-free.sh
```

When prompted, select the event number.

```shell
$ ./calendly-free.sh
     1  Support - 30 Minute Meeting
     2  Support - One hour Meeting

Enter the number of the event you want to schedule:
1

Available times for Support - 30 Minute Meeting
------------------------
14/01/25: 10:45, 11:15, 16:15, 16:45
15/01/25: 09:15, 09:45, 14:15, 14:45, 15:15
16/01/25: 11:15, 11:45
17/01/25: 10:15, 14:15, 14:45, 15:15, 15:45, 16:15, 16:45
20/01/25: 09:15, 13:45, 14:15, 14:45, 15:15, 15:45, 16:15, 16:45
------------------------
Fetch another meeting type, or quit? (y/n) n
Exiting..
```

## Contributing

P/MRs are welcome! For major changes, please open an issue first to discuss what you would like to change.
