#!/usr/bin/env ruby
# frozen_string_literal: true

# ## Download multiple CI job logs
#
#  To then run:
#   1. https://gitlab.com/gitlab-com/support/toolbox/list-slowest-job-sections/
#   2. clean_job_logs_for_diff_ing.sh
#   3. diff CI-123.log CI-456.log
#
# USAGE: Read the --help flag output for details.
#
# EXAMPLES:
#
#   LAB_CORE_TOKEN=...
#   gls_download_ci_logs --project 30654121 \
#     3343446459 3343405553
#
#   ./download_ci_logs.rb -p 30654121 --label=fast 3343446459
#   ./download_ci_logs.rb -p 30654121 --label=slow 3343405553
#
#   ruby download_ci_logs.rb -p 9450192 --token=... \
#     --skipped --pipeline 1025018397 1025018388

require 'gitlab'
require 'optimist'

# rubocop:disable Layout/LineLength
opts = Optimist.options do
  banner 'USAGE: download_ci_logs.rb [OPTIONS] id1 [id2 [...]]'
  opt :host, 'GitLab instance hostname',
    type: :string, default: 'gitlab.com'
  opt :token, 'Personal Access Token with read_api scope. Can also use environment variables GITLAB_API_PRIVATE_TOKEN or LAB_CORE_TOKEN.',
    type: :string
  opt :project, 'Numeric project ID in which the jobs ran.',
    type: :integer
  opt :pipeline, 'Interpret IDs as pipelines, and download all their job logs.',
    type: :bool, default: false
  opt :skipped, 'Include skipped jobs. The resulting empty files only seem useful for the sake of completeness.',
    type: :bool, default: false
  opt :label, 'Add a label to filename that aids later analyses (examples: slow, fast, old, new, etc.)',
    type: :string
end
# rubocop:enable Layout/LineLength

# Default behaviour: Loop through input parameters as job IDs.
class CiLogDownloader
  attr_reader :opts

  ID_REGEX = /\A\d+\z/

  def initialize(opts)
    @opts = opts

    Gitlab.configure do |config|
      config.endpoint = "https://#{opts[:host]}/api/v4"
      config.private_token = opts[:token] || ENV['GITLAB_API_PRIVATE_TOKEN'] || ENV.fetch('LAB_CORE_TOKEN', nil)
    end
  end

  def download_logs!
    ARGV.grep(ID_REGEX).each do |id|
      job ||= job(id)
      next if !opts[:skipped] && (job.status.eql? 'skipped')

      name = log_file_name(job)
      puts "Downloading #{name} ..."

      file = File.new(name, 'w')
      file.write(log_header(id, job))
      file.close
    end
  end

  private

  def log_header(id, job)
    name = job.name.gsub(/[^[:alnum:][:blank:][:punct:]]/, '').squeeze(' ').strip
    # Extract non-emoji character to avoid Encoding::CompatibilityError
    # https://stackoverflow.com/a/52652437

    "# Job name: #{name}\n\n#{log(id)}"
  end

  def log(id)
    Gitlab.job_trace(opts[:project], id)
  end

  def job(id)
    Gitlab.job(opts[:project], id)
  end

  def log_file_name(job, pipeline_id = nil)
    name = ["CI"]
    name << opts[:label] if opts[:label]
    name << job.status.to_s
    name << "#{pipeline_id}p" if pipeline_id # unless pipeline_id.nil?
    name << "#{job.id}j.log"
    name.join('-')
  end
end

# With the --pipeline flag, we need an outer loop over all pipeline IDs.
class PipelineDownloader < CiLogDownloader
  def download_logs!
    ARGV.grep(ID_REGEX).each do |id|
      pipeline_jobs(id).each do |job|
        next if !opts[:skipped] && (job.status.eql? 'skipped')

        name = log_file_name(job, id)
        puts "Downloading #{name} ..."

        file = File.new(name, 'w')
        file.write(log_header(job.id, job))
        file.close
      end
    end
  end

  private

  def pipeline_jobs(id)
    Gitlab.pipeline_jobs(opts[:project], id, include_retried: true).auto_paginate
  end
end

if opts[:pipeline]
  PipelineDownloader.new(opts).download_logs!
else
  CiLogDownloader.new(opts).download_logs!
end
