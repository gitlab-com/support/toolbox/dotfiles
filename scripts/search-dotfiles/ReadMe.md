# Search the GitLab Support team's dotfiles

Result of [meta#3796](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3796).
(How to provide commonly useful shell aliases, functions, scripts, etc. to the whole team?)

This script helps you quickly find those items that team members use to:

- analyze GitLab logs
- save our hands from repetitive strain injury
- etc.

## Installation

1. See [top-level ReadMe](../../README.md#installation)
2. `cd path/to/search-dotfiles && bundle install`

## Usage

```shell
GITLAB_API_PRIVATE_TOKEN=<with-read-API-scope> search_dotfiles '1 search string' 
```

### Example

:thinking_face: Which tweaks do colleagues use to handle customer's SOS archives?

```shell
$ export GITLAB_API_PRIVATE_TOKEN=glpat-… # or LAB_CORE_TOKEN
$ ./search_dotfiles.rb SOS

┌─ …
│  …
│  #!/usr/bin/env bash
│  set -ux
│  …
└─ https://gitlab.com/greg/scripts/-/blame/9067b41a/sos-alyzer.sh

┌─ …
│  …
│  # Context: https://www.chezmoi.io/docs/templating/#template-syntax
│  
│  # This script helps me extract basic data from GitLabSOS … truncated, at 72 characters
│  
│  ### REQUIREMENTS
│  …
└ https://gitlab.com/katrinleinweber/dotfiles/-/blame/36453228/dot_config/zd.sh.tmpl
```

### Caveats

1. Often, a result's _preview snippet_ is too short to be useful.
   This is due to the returned number of lines in
   [the `data` field of the search API](https://docs.gitlab.com/ee/api/search.html#scope-blobs).
   However, the URL is printed in all cases and should make it easy to find out more.

## Contributing

MRs are welcome! For major changes, please first open an issue to discuss your suggestion.

We'd be especial glad if you added your own dotfiles or scripts repository to
[`repos.yaml`](repos.yaml).
Please keep the list sorted alphabetically.

Use [lefthook's `install` command](https://github.com/evilmartians/lefthook/blob/master/docs/full_guide.md)
to benefit from [some automated, hook-based actions](lefthook.yml) when developing.

To document any user-facing changes, please use expressive commit messages,
and then [git-extras' `changelog` command](https://github.com/tj/git-extras/blob/master/Commands.md#git-changelog).

## License: [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

Copyright (c) 2021 Katrin Leinweber (GitLab Inc.)
