0.1.0 / 2021-11-18
==================

  * Separate general & script-specific install documentation
  * Log changes (automatically) via "git changelog"
  * Document lefthook usage & benefits
  * Report empty search results list explicitly
  * Guard against no or too many search terms
  * Add nice, random results header
  * Register more colleagues's dotfile repos
  * Format output box in a more robust way
  * Check code quality gems during development
  * Document an example & caveats
  * Document 2 install variants
  * Parallelize API calls to speed up reporting of results
  * Initial commit of structurally tested, loop-based search
  * Require 2 approvals of senior support engineers
  * Initial commit
