#!/usr/bin/env ruby
# frozen_string_literal: true

require 'gitlab'
require 'parallel'

# See https://gitlab.com/gitlab-com/support/toolbox/dotfiles/-/blob/main/scripts/search-dotfiles/ReadMe.md
class DotFileSearch
  attr_reader :term, :base, :client, :config, :endp, :pat

  FILE = File.symlink?(__FILE__) ? File.realdirpath(__FILE__) : __FILE__
  DIR  = File.expand_path(File.dirname(FILE))

  TERMINAL_WIDTH    = 72
  NICE_RESULT_INTRO =
    [
      'Your awesome colleagues shared:',
      'Does any of these help you move forward?',
      'Great minds think alike!',
      'Such bash! Much choice!'
    ].sample

  def initialize(term)
    @apiv4  = '/api/v4'
    @base   = 'https://gitlab.com'
    @config = "#{DIR}/repos.yaml"
    @term   = term

    # We'll be reusing both the possibly existing https://github.com/zaquestion/lab#configuration
    # and https://gitlab.com/gitlab-org/quality/triage-serverless/-/blob/master/triage/triage.rb
    @endp = ENV.fetch('GITLAB_API_ENDPOINT', nil) || ((ENV.fetch('LAB_CORE_HOST', nil) || @base) + @apiv4)
    @pat = ENV.fetch('GITLAB_API_PRIVATE_TOKEN', nil) || ENV.fetch('LAB_CORE_TOKEN', nil)
    @client = Gitlab.client(endpoint: endp, private_token: pat)
  end

  def search
    responses = Parallel.map(repos, in_threads: 8) do |repo|
      hits = begin
        client.search_in_project(repo, 'blobs', term) # array
      rescue Gitlab::Error::NotFound => e
        puts "\nCannot search   #{repo}\n#{e}"
        []
      end
      extract_data_of(hits, repo) unless hits.empty?
    end
    results = responses.flatten.compact
    report(results)
  end

  private

  def repos
    YAML.load_file(config).fetch('repos')
  end

  def linenumber(string)
    string.split("\n").length
  end

  # https://stackoverflow.com/a/46200369/4341322
  def longest(string)
    string.split("\n").max_by(&:length)[0].length
  end

  # https://stackoverflow.com/a/19048598/4341322
  def truncate(line)
    line.length > TERMINAL_WIDTH ? "#{line[0...TERMINAL_WIDTH]} …" : line
  end

  def nothing_found
    puts <<~HEREDOC
      \nNothing found for   #{term}   :-/
      \nIf you've built something relevant, please share it in
      https://gitlab.com/gitlab-com/support/toolbox/dotfiles
    HEREDOC
  end

  def abbrev_sha1(string)
    if string.match?(/\A[0-9a-f]{40}\z/)
      string.slice(0, 8)
    else
      string
    end
  end

  def extract_data_of(hits, from)
    hits.collect do |h|
      OpenStruct.new(
        repo: from,
        file: h['filename'],
        ref: abbrev_sha1(h['ref']),
        result: h['data']
      )
    end
  end

  # Similar to https://gitlab.com/kl-das/from-scratch-test-runner/-/blame/02fc7555/runner.rb
  def report(results)
    return nothing_found if results.empty?

    puts "\n#{NICE_RESULT_INTRO}\n"
    results.each do |r|
      puts "\n"
      puts '┌───'
      puts '│  …'
      puts r.result.split("\n").map { |line| "│  #{truncate(line)}" }
      puts '│  …'
      puts "└─ #{base}/#{r.repo}/-/blame/#{r.ref}/#{r.file}\n"
    end
  end
end
DotFileSearch.new(ARGV.pop).search
