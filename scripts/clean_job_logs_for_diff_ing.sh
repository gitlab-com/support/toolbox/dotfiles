#!/bin/bash

### Clean job logs for diff-ing
#
# To obtain logs, use download_ci_logs.rb
#
# Remove strings that introduce non-useful differences into job logs
# and are encountered relatively often by GitLab Support.
#
# ### REQUIREMENT: https://github.com/chmln/sd#sd---search--displace
#
# ### USAGE
#
#   gls_clean_jobs_log_for_diff_ing fail-*.log succeeded-*.log
#
# ## CAVEAT
#
# Run https://gitlab.com/gitlab-com/support/toolbox/list-slowest-job-sections/
# before cleaning the logs.

for file in "$@"; do
  echo "Cleaning $file ..."

  # Timestamps that match ISO8601 plus timezone # based on https://regex101.com/library/zZ1pF9
  sd '\d{4}-\d{2}-\d{2}(\s|T)\d{2}:\d{2}:\d{2}(\.\d+)? ?\+\d{2}:?\d{2}' '…time…' "$file"

  # Timestamps from GitLab CI sections
  sd ':\d{10}:' '…time…' "$file"

  # Timestamps from GIT_CURL_VERBOSE, GIT_TRACE & GIT_TRACE_*
  sd '\d{2}:\d{2}:\d{2}/.\d+' '…time…' "$file"

  # Progress indicators for Git fetches
  sd 'Fetched .+' '…git…fetch…' "$file"

  # Progress indicators for Git # keeping first & last lines to see object counts & speeds
  sd 'Filtering content:  \d{2}.+$' '…git…filtering…content…' "$file"

  # Progress indicators for apt-get
  sd '(Get|Hit):\d+ .+B\]' '…apt…get…hit…' "$file"
  sd '(Preparing|Unpacking|Selecting|Setting) .+\.' '…apt…prep…unpack…select…set' "$file"

  # Progress indicators for Maven
  sd 'Progress \(\d+\): .+' '…maven…progress…' "$file"

  # Secret( block)s, also as backstop if customers didn't sanitze them
  sd --flags s "'-----BEGIN (CERTIFICATE|OPENSSH).+(CERTIFICATE|KEY)-----'" '…certificate…' "$file"
done

echo -e "\nClean up further by copy-paste-ing and completing this command:\n\n  sd '' '……' $*"
