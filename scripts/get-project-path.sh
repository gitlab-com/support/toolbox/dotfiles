#!/usr/bin/env bash

# Gets the hashed project path for a project ID
#
# USAGE:
# ❯ get-project-path 635
#
# OUTPUT:
# /var/opt/gitlab/git-data/repositories/@hashed/26/18/2618182c3894875e16eeafa6c24e1fe926150ebc6403980c2cb1bbff192d296d.git

get-project-path() {
  PROJECT_HASH=$(echo -n "$1" | sha256sum | cut -d ' ' -f 1)
  echo "/var/opt/gitlab/git-data/repositories/@hashed/${PROJECT_HASH:0:2}/${PROJECT_HASH:2:2}/${PROJECT_HASH}.git"
}

get-project-path "$1"
