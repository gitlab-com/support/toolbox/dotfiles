# Generate a single-use Calendly link

## Installation

0. Follow the Calendly account setup as described on
   [our handbook's Calendly page](https://about.gitlab.com/handbook/support/workflows/calendly.html).
1. Create a [token for their API](https://calendly.com/integrations/api_webhooks).
2. (Optional) To use with 1password-cli, install
   [1Password CLI package](https://app-updates.agilebits.com/product_history/CLI2). Version 2 is a requirement.

## Usage

### To provide Calendly API token **directly** use:

- either the environment variable `CALENDLY_TOKEN`,
- or the first argument that is passed in:

```shell
export CALENDLY_TOKEN="…"
./generate-single-use-calendly-link.sh

# or
./generate-single-use-calendly-link.sh "…"
```

### To use **1password-cli** to get the token instead:

1. Create a new login item named `CALENDLY_TOKEN` in your `Private` 1password vault, put the token into the `password` field
1. [Add your 1Password account to 1password cli](https://developer.1password.com/docs/cli/sign-in-manually)
1. Make sure to set `DOTFILES_USE_1PASSWORD=true` environment variable

```shell
export DOTFILES_USE_1PASSWORD=true
gls_calendly_link
# or
./generate-single-use-calendly-link.sh
```

When prompted, select the event number.

## Contributing

P/MRs are welcome! For major changes, please open an issue first to discuss what you would like to change.
