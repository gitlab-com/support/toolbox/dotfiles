#!/usr/bin/env bash

# Get Calendly URL for an appointment scheduling webpage
# via https://about.gitlab.com/handbook/support/workflows/calendly.html#using-curl

# Support for 1Password to get CALENDLY_TOKEN:
# * to use make sure to add `export DOTFILES_USE_1PASSWORD=true` to .bashrc
# * also, have 1password cli version 2 installed

OP_VERSION=$(op --version 2>/dev/null)

if [[ "$1" == "" ]] && [[ "$CALENDLY_TOKEN" == "" ]] && [[ "$OP_VERSION" =~ ^2\. ]] && [[ "$DOTFILES_USE_1PASSWORD" == "true" ]]; then
  # Use 1Password CLI only if $1 and $CALENDLY_TOKEN are not set
  op account get >/dev/null 2>&1 || eval "$(op signin)"
  CALENDLY_TOKEN=$(op item get CALENDLY_TOKEN --vault Private --fields=password --reveal)
fi

PAT=${1:-"$CALENDLY_TOKEN"}
# https://stackoverflow.com/a/24405696/4341322

# TODO: flag for meeting name
# TODO: default: list events

HEADER="Authorization: Bearer $PAT"

USER_URL=$(
  curl --silent -H "$HEADER" \
    "https://api.calendly.com/users/me" |
    jq --raw-output '.resource.uri'
)

RESULT=$(curl --silent -H "$HEADER" --get --data user="$USER_URL" "https://api.calendly.com/event_types?active=true")

echo "$RESULT" | jq -r '.collection[] | [.uri, .name] | @tsv' | cat -n

echo -e "\nEnter the number of the event you want to schedule:"
read -r selection

index=$((selection - 1))
URI=$(echo "$RESULT" | jq -r ".collection | .[$index] | .uri")

LINK=$(
  curl --silent -H "$HEADER" \
    --form max_event_count=1 \
    --form owner_type=EventType \
    --form owner="$URI" \
    https://api.calendly.com/scheduling_links |
    jq --raw-output \
      '.resource.booking_url'
)

echo -e "\nYour single-use link:\n$LINK"
