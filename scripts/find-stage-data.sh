#!/usr/bin/env bash

# Retrieves the names of certain roles for product groups
#
# USAGE:
# ❯ find-stage-data.sh pm ~/code/www-gitlab-com Geo
#
# OUTPUT:
# Sampath Ranasinghe
# (the PM for the Geo group)

rolekey=$1 # e.g. pm, tech_writer, ux
shift
wwwgitlabfolder=$1 # e.g. ~/code/www-gitlab-com
shift

# using shift allows us to use "$*" (all parameters as one string) below,
# so you can do "tw pipeline authoring" instead of "tw 'pipeline authoring'"

yq \
  '.stages.[].groups.[] | select(.name | test("(?i)'"$*"'")) | {.name: [] + .'"$rolekey"' | join(", ")}' \
  "$wwwgitlabfolder/data/stages.yml"

# print warning in case of stale local data (file not changed for 14 days = 1036800 seconds)
filemtime=$(date -r "$wwwgitlabfolder/data/stages.yml" +"%s")
currenttime=$(date +"%s")
diff=$(expr $currenttime - $filemtime)
if [ $diff -ge 1036800 ]; then
  echo "WARNING: Local file <$wwwgitlabfolder/data/stages.yml> not changed within 14 days, consider pulling recent changes from remote."
fi
