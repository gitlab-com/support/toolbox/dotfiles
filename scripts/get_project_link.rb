#!/usr/bin/env ruby
# frozen_string_literal: true

# Build Project URL for any file or directory within a repository

require 'open3'
require 'optimist'
require 'uri'

options = Optimist.options do
  banner = 'Usage: get_project_link.rb [options]'
  opt :name, 'Name of subject, could be a file or directory',
      short: '-n', type: :string
  opt :permalink, 'Generate permalink',
      short: '-p', default: false
  opt :lines, 'Specify line(s) to add as URL anchor',
      short: '-l', type: :string
  opt :url, 'Specify Project URL to parse and open locally',
      short: '-u', type: :string
  opt :editor, 'Specify editor to open local file resolved from URL',
      short: '-e', type: :string
  opt :blame, 'Generate blame URL',
      short: '-b', default: false
  opt :commits, 'Generate commit history link',
      short: '-c', default: false
  conflicts :commits, :url, :lines
  conflicts :commits, :url, :blame
end

REMOTE_URL_REGEX = %r{^(?<protocol>https|http|git|ssh)(://|@)*(?<url>[^:\/]+)([\/:])*(?<namespace>.*)\.git}
GITLAB_URI_PATH_REGEX = %r{(?<namespace>.*)/-/(blob|tree|blame)/(?<ref_and_file_path>[^?#]+)$}

def build_url(remote_url, options)
  matches = remote_url.match REMOTE_URL_REGEX

  return nil unless matches

  ref = resolve_ref(options[:permalink])['ref']
  object_type = resolve_object_type(options)
  object_path = resolve_relative_path(options[:name]) if options[:name]

  url_path = format('/%s/-/%s/%s/%s', matches[:namespace], object_type, ref, object_path)

  url_hash = {
    host: matches[:url],
    path: url_path
  }

  url_hash[:fragment] = "L#{options[:lines]}" if options[:lines] and !%w[tree commits].include?(object_type)
  URI::HTTPS.build(url_hash)
end

def branch_exists?(ref)
  result, status = Open3.capture2("git show-ref --verify refs/heads/#{ref} 2> /dev/null")
  result = result.delete!("\n")
end

def tag_exists?(ref)
  result, status = Open3.capture2("git show-ref --verify refs/tags/#{ref} 2> /dev/null")
  result = result.delete!("\n")
end

def sha_exists?(ref)
  get_ref_sha("#{ref}^{commit}")
end

def get_remote_origin_url
  remote_url, status = Open3.capture2('git config --get remote.origin.url 2> /dev/null')
  remote_url.delete!("\n")
end

def get_current_branch
  branch, status = Open3.capture2('git branch --show-current 2> /dev/null')
  branch.delete!("\n")
end

def get_current_tag
  tag, status = Open3.capture2('git describe --tags --exact-match 2> /dev/null')
  tag.delete!("\n")
end

def get_head_sha
  get_ref_sha('HEAD', false)
end

def get_ref_sha(ref, verify = false)
  if verify
    sha, status = Open3.capture2("git rev-parse #{ref} --verify 2> /dev/null")
  else
    sha, status = Open3.capture2("git rev-parse #{ref} 2> /dev/null")
  end

  sha.delete!("\n")
  sha
end

def parse_url_ref_and_file_path(ref_and_file_path)
  source = ref_and_file_path
  file_path = ref_and_file_path
  refs = ref_and_file_path.split('/')
  check_ref = ''
  match = nil

  # Crawl until ref no longer matches
  # Avoids partial ref matches, gets best match
  # Used to differentiate ref and file path
  # when refs may legitimately contain forward slashes
  for r in refs do
    current_match = nil
    check_ref = check_ref.split('/').push(r).join('/')

    if is_ref_sha?(check_ref) and sha_exists?(check_ref)
      current_match = 'sha'
    elsif branch_exists?(check_ref)
      current_match = 'branch'
    elsif tag_exists?(check_ref)
      current_match = 'tag'
    end

    break if current_match.nil? and !match.nil?

    ref = check_ref
    match = current_match
  end

  file_path.slice! "#{ref}/"

  {
    'ref' => ref,
    'ref_type' => match,
    'file_path' => file_path
  }
end

def is_ref_sha?(ref)
  is_ref_sha = ref.match(/^[0-9a-f]{40}$/)
  !is_ref_sha.nil?
end

def sanitise_uri(uri)
  uri.query = nil
  return uri
end

def parse_url_opt(url)
  # Use URI gem to parse URI
  begin
    uri = URI(url)
    sanitised_uri = sanitise_uri(uri)
  rescue => e
    puts "ERROR: could not parse URL: #{e.message}"
  end

  # Check if URI path matches expected regex
  matches = sanitised_uri.path.match GITLAB_URI_PATH_REGEX

  return nil unless matches

  # Parse ref and namespace from URL
  url_namespace = matches[:namespace][1..-1]
  ref_and_file_path = parse_url_ref_and_file_path(matches[:ref_and_file_path])
  ref = ref_and_file_path['ref']
  file_path = ref_and_file_path['file_path']
  line = sanitised_uri.fragment.split('L')[1].split('-')[0] if sanitised_uri.fragment

  name = ref if ref_and_file_path['ref_type'] == 'sha'

  name = nil if name == 'remotes/origin/HEAD'

  {
    'url_namespace' => url_namespace,
    'ref' => ref,
    'ref_type' => ref_and_file_path['ref_type'],
    'ref_name' => name,
    'file_path' => file_path,
    'lines' => line
  }
end

def invoke_editor(editor, file_path, line)
  editor, status = Open3.capture2("which #{editor}")
  editor.strip!

  line = 1 if line.nil?

  if editor =~ %r{/{0,3}vi?} || editor =~ /.*macs.*/
    exec "#{editor}", "+#{line}", file_path
  elsif editor =~ %r{/cod+}
    exec "#{editor}", '-g', "#{file_path}:#{line}"
  elsif editor =~ %r{/subl}
    exec "#{editor}", "#{file_path}:#{line}"
  end
end

def resolve_ref(permalink = false)
  branch = get_current_branch
  tag = get_current_tag
  head_sha = get_head_sha

  is_branch = !!branch
  is_tag = !!tag

  if permalink
    ref = head_sha
    ref_type = 'sha'
  elsif is_branch
    ref = branch
    ref_type = 'branch'
  elsif is_tag
    ref = tag
    ref_type = 'tag'
  else
    ref = head_sha
    ref_type = 'sha'
  end

  {
    'ref' => ref,
    'type' => ref_type
  }
end

def resolve_object_type(options)
  object_type = 'tree'

  if options[:name]
    if File.directory?(options[:name])
      object_type = 'tree'
    elsif File.file?(options[:name])
      object_type = 'blob'
    end

    if options[:commits]
      object_type = 'commits'
    elsif options[:blame]
      object_type = 'blame'
    end

  end

  object_type
end

def get_git_relative_pwd
  pwd_relative, status = Open3.capture2('git rev-parse --show-prefix')
  pwd_relative.delete!("\n")
end

def resolve_relative_path(file_path)
  path = file_path
  relative_git_path = get_git_relative_pwd
  path_array = relative_git_path.split('/')
  backwards_steps = file_path.scan(/\.\./)

  backwards_steps.each do |_b|
    path_array.pop
  end

  relative_git_path = File.join(path_array)

  new_path_array = []
  new_path_array.append(relative_git_path) if relative_git_path.length > 1

  new_path_array.append(file_path)

  File.join(new_path_array).gsub('../', '')
end

def checkout_ref(ref)
  checkout, status = Open3.capture2("git checkout #{ref}")

  status
end

def get_sha_name(sha)
  name, status = Open3.capture2("git name-rev #{sha} --name-only")

  return nil unless status.success? and name

  name = name.delete!("\n")
  name = name.split('~')[0]
  name.strip!

  name
end

if options[:url]
  parsed_url = parse_url_opt(options[:url])

  # Get remote namespace from git config
  remote_url = get_remote_origin_url
  matches = remote_url.match REMOTE_URL_REGEX
  remote_namespace = (matches[:namespace] unless matches.nil?)

  namespace_match = remote_namespace == parsed_url['url_namespace']

  editor = options[:editor] || ENV['EDITOR']

  if parsed_url.nil?
    "ERROR: Could not parse URL"
  end

  if !namespace_match
    puts "ERROR: URL namespace does not match remote namespace."
    puts "Remote namespace: #{remote_namespace}"
    puts "URL namespace: #{parsed_url['url_namespace']}"
    return
  end

  unless editor
    puts "INFO: Editor not specified, so just printing local file path"
    puts parsed_url['file_path']
    return
  end

  verify_ref_exists = get_ref_sha(parsed_url['ref'], true)

  if verify_ref_exists
    local_ref = resolve_ref
    local_ref_value = local_ref['ref']
    local_ref_type = local_ref['type']

    local_ref_value = "tags/#{local_ref_value}" if local_ref_type == 'tag'

    if parsed_url['ref_name'] != local_ref_value and get_head_sha != parsed_url['ref']
      checkout_ref = parsed_url['ref_name'] || parsed_url['ref']
      puts "Do you want to checkout ref #{checkout_ref}? (y/n)"
      answer = gets.chomp.downcase
      checkout_ref(checkout_ref) unless answer != 'y'
    end

    if !File.exist?(parsed_url['file_path'])
      puts "File does not exist at path: #{parsed_url['file_path']}"
      return
    end

    invoke_editor(editor, parsed_url['file_path'], parsed_url['lines'])

  end

end

return if options[:name] && !File.exist?(options[:name])

remote_url = get_remote_origin_url
return unless remote_url

url = build_url(remote_url, options)

return if url.nil?

puts url
