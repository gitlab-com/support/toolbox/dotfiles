<!--
Re-using the main template: https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/merge_request_templates/Default.md
But trimmed a bit
-->

## What does this MR do and why?

%{first_multiline_commit}

## Screenshots, recordings or output

| Before | After  |
| ------ | ------ |
|        |        |

## How to set up and validate locally

<!--
Numbered steps should be in the script file itself or a dedicated ReadMe, both of which should be useful for our users.
Alternatively, a Support-Engineer-focussed list of steps can be used here with justification.
-->

## MR acceptance checklist

- [ ] I've tested this change on: macOS/Linux.
- [ ] This change needs no other test and thus no review, because: …justification….
      I'll merge it myself.
  - [ ] This _does need_ at least 1 additional test on another machine, ideally using a different OS.
        I've facilitated this via: [Slack thread](…url…) / MR review.

/assign me
