# Shared Support Dotfiles

A shared repository for aliases, functions, scripts, and snippets we Support folks found helpful.

## Dependencies

Are all defined at the root level: [.tool-versions](./.tool-versions), [Gemfile](./Gemfile) & [Brewfile](./Brewfile).
Install them all with [asdf](https://asdf-vm.com/), [Bundler](https://bundler.io#getting-started) & [Homebrew](https://brew.sh/):

```shell
asdf install && bundle install && brew bundle install --no-lock --no-upgrade
```

Additionally, several helper tools are required for the [contribution workflow](#contributing).

## Installation

First, clone this repo and `cd` into it. Then:

- Either: Add `export PATH="$(pwd)/bin:$PATH"` in your `.{ba,z}shrc`.
  - This way, only a `git pull` in your clone of this repo is needed for updating.
- Or: Manually symlink each script: `ln -s scripts/file.ext /usr/local/bin/name`
  - This gives you control over how to call them locally,
    but requires manual updates whenever files are renamed upstream, or new scripts are added.

Alternative methods to integrate this repo's various contents into personal dotfiles:

1. Just copy-paste what you like ¯\\\_(ツ)_/¯
1. `git clone git@gitlab.com:gitlab-com/support/toolbox/dotfiles.git` to a location that you then source from your `.bashrc`, `.zshrc`, etc.

    ```shell
    # cat path/to/support-toolbox-dotfiles/aliases/* > ~/.shared-aliases   # every now and then
    source ~/.shared-aliases
    ```

If you use your own dotfiles repo, you can also run
`git remote add --no-fetch support-toolbox git@gitlab.com:gitlab-com/support/toolbox/dotfiles.git` in it, and try:
    - `git cherry-pick <any-commit-that-creates-a-file>`
    - `git restore --source=support-toolbox/main path/to/file`
    - `git merge --allow-unrelated-histories support-toolbox/dotfiles.git main`
    - TODO: examples for other [dotfiles.GitHub.io/utilities](https://dotfiles.github.io/utilities/)

### Usage

Available Item | Purpose
-- | --
[browse-commit-page] | Browse from a Git hash in iTerm2 to the commit's webpage.
[calendly-free] | List your free Calendly meeting times.
[calendly-link] | Generate the Calendly URL of an appointment scheduling widget.
[clean_job_logs_for_diff_ing] | Remove content from CI logs that's a distraction while `diff`-ing (`passed` vs. `failed`, for example).
[compose_docker_instance] | Start a ticket-specific, Docker-based test instance (not on M1, [yet](https://gitlab.com/groups/gitlab-org/-/epics/2370)).
[diff_nfs_mount_options] | Diff a customer's NFS mount options against our recommendations.
[download_ci_logs] | Download multiple CI job logs (or all of 1 or more pipelines), for example to then [list their slowest sections](https://gitlab.com/gitlab-com/support/toolbox/list-slowest-job-sections/) and/or [diff them][clean_job_logs_for_diff_ing]
[emoji_reaction] | Easily add [custom reaction emoji](https://gitlab.com/custom_emoji/custom_emoji/-/issues/1) to issues, MRs or comments.
[find-product-manager] & [find-technical-writer] | Find the names for those roles in [www-gitlab-com's stages.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml)
[find-stage-data] & [find-stage-data-remote] | Generic scripts to implement specific search scripts like above. The `remote` version has to be set up via your own aliases, the provided search scripts above use the non-remote version.
[generate_commits] | In a Git repo, create small or large files, commit and push them (optionally to LFS).
[get_project_link] | Build the project URL for any file or directory within a repository.
[log_stats] | Allows to generate stats from Json type logs. Similar to fast-stats but more flexible and slower
[read_release_posts] | Read all release posts and patch notes for a given `Major.ninor` version.
[request-for-help-issue] | Find the new issue with template URL for a product group.
[search_default_charts_values_version] | Search `values.yaml` of a given Chart version.
[search_default_gitlab_rb_version] | Search `gitlab.rb` of a given Omnibus version.
[search_default_structure_sql_version] | Search `structure.sql` of a given GitLab version.
[search_sidekiq_worker_data] | Search Sidekiq's `{ee/}app/workers/all_queues.yml` data
[search-dotfiles] | Search the dotfiles repos of various GitLab team members.

[browse-commit-page]: ./scripts/browse-commit-page/ReadMe.md
[calendly-free]: ./scripts/calendly-free/ReadMe.md
[calendly-link]: ./scripts/calendly-link/ReadMe.md
[clean_job_logs_for_diff_ing]: ./scripts/clean_job_logs_for_diff_ing.sh
[compose_docker_instance]: ./scripts/compose_docker_instance.rb
[diff_nfs_mount_options]: ./scripts/diff_nfs_mount_options.sh
[download_ci_logs]: ./scripts/download_ci_logs.rb
[emoji_reaction]: ./scripts/emoji_reaction.rb
[find-product-manager]: ./scripts/find-product-manager.sh
[find-technical-writer]: ./scripts/find-technical-writer.sh
[find-stage-data]: ./scripts/find-stage-data.sh
[find-stage-data-remote]: ./scripts/find-stage-data-remote.sh
[generate_commits]: ./scripts/generate_commits.rb
[get_project_link]: ./scripts/get_project_link.rb
[log_stats]: ./scripts/log_stats.rb
[read_release_posts]: ./scripts/read_release_posts.sh
[request-for-help-issue]: ./scripts/request-for-help-issue.rb
[search_default_charts_values_version]: ./scripts/search_default_gitlab_rb_version.sh
[search_default_gitlab_rb_version]: ./scripts/search_default_gitlab_rb_version.sh
[search_default_structure_sql_version]: ./scripts/search_default_structure_sql_version.sh
[search_sidekiq_worker_data]: ./scripts/search_sidekiq_worker_data.sh
[search-dotfiles]: ./scripts/search-dotfiles/ReadMe.md

For a general refresh on how to use dotfiles, read this:
<https://medium.com/@webprolific/getting-started-with-dotfiles-43c3602fd789>

### Support

Create an issue and assign or mention any of the instigators
([support-pairing#4334](https://gitlab.com/gitlab-com/support/support-pairing/-/issues/4334)):

- @greg
- @b_freitas
- @katrinleinweber

If the issue is about a specific script or file, check its blame view
and assign to a frequent and/or recent contributor.

### Contributing

MRs are welcome! For major changes, please open an issue first to discuss what you would like to change.

Each alias and function should exist in a separate file. To add a new alias or function, create a new file with filename matching the alias or function name in the appropriate directory.

To make the contribution workflow cleaner:

1. Install the following additional dependencies:
   - [gitleaks](https://github.com/zricethezav/gitleaks/)
     - Note that you can also [install this system-wide][glei],
       but it would disable our other Lefthooks.
   - [shellcheck](https://github.com/koalaman/shellcheck#installing)
   - [shfmt](https://github.com/mvdan/sh#shfmt)
1. [Install and initialize Lefthook][iilh].
   Upon `git commit`ing and other operations output like this should appear:
   `Lefthook v… RUNNING HOOKS …`

To learn more, read about:

- Our automatically enforced rules: [lefthook.yml](Lefthook.yml) and
  [the `.lefthook/**` scripts](.lefthook).
- Lefthook tweaks: [GDK docs][gdkd].
- Why Lefthook was introduced in GitLab generally: [gitlab#220807][gl07].

[iilh]: https://github.com/evilmartians/lefthook/blob/master/docs/full_guide.md#installation
[glei]: https://gitlab.com/gitlab-com/gl-security/security-research/gitleaks-endpoint-installer
[gdkd]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/contributing/style_guides.md#pre-push-static-analysis-with-lefthook
[gl07]: https://gitlab.com/gitlab-org/gitlab/-/issues/220807
